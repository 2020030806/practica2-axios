const express = require('express')
const router = express.Router()
const ejs = require('ejs');
const bodyparser = require('body-parser')
const db = require('../models/alumno.js')
const axios = require('axios')  

let alumno = {
    Matricula: "",
    Nombre: "",
    Domicilio: "",
    Sexo: "",
    Especialidad: ""
}

let campos = {
    Matricula: "",
    Nombre: "",
    Domicilio: "",
    Sexo: "",
    Especialidad: ""
}



router.post('/insertar',async(req,res)=>{
    
    alumno = {
        Matricula: req.body.Matricula,
        Nombre: req.body.Nombre,
        Domicilio: req.body.Domicilio,
        Sexo: req.body.Sexo,
        Especialidad: req.body.Especialidad
    }   

    let resultado = await db.insertar(alumno)
    res.json(resultado)
})

router.get('/consultar',async(req,res)=>{

    let resultado = await db.consultar()
    res.json(resultado)
})

router.post('/consultarMatricula',async(req,res)=>{

    Matricula = req.body.Matricula
    let resultado = await db.consultarMatricula(Matricula)
    res.json(resultado)
})

router.post('/eliminar',async(req,res)=>{

    Matricula = req.body.Matricula
    let resultado = await db.eliminar(Matricula)
    res.json(resultado)
})

router.post('/actualizar',async(req,res)=>{

    alumno = {
        Matricula: req.body.Matricula,
        Nombre: req.body.Nombre,
        Domicilio: req.body.Domicilio,
        Sexo: req.body.Sexo,
        Especialidad: req.body.Especialidad
    }   
    let resultado = await db.actualizar(alumno)
    res.json(resultado)
})





















router.get('/',(req,res)=>{

    axios({
        method: 'get',
        url: 'http://localhost:500/consultar',
    })
        .then(function (response) {
            res.render('index.html',{titulo: "Listado de alumnos", listado: response.data, campos:campos})
        });
})

router.post('/consultarPorMatricula',async (req,res)=>{
    Matricula = req.body.Matricula
    let resultado = await db.consultarMatricula(Matricula)
    let lista = await db.consultar()
    res.render('index.html',{titulo: "Listado de alumnos", listado: lista, campos:resultado})
    
})

router.get('/agregar',(req,res)=>{

    axios({
        method: 'post',
        url: 'http://localhost:500/insertar',
        data: {
            Matricula: req.query.Matricula,
            Nombre: req.query.Nombre,
            Domicilio: req.query.Domicilio,
            Sexo: req.query.Sexo,
            Especialidad: req.query.Especialidad
        }
    })
        .then(function (response) {
            res.redirect('/')
        });
})

router.get('/borrar',(req,res)=>{

    axios({
        method: 'post',
        url: 'http://localhost:500/eliminar',
        data: {
            Matricula: req.query.Matricula
        }
    })
        .then(function (response) {
            res.redirect('/')
        });
})
router.get('/modificar',(req,res)=>{

    axios({
        method: 'post',
        url: 'http://localhost:500/actualizar',
        data: {
            Matricula: req.query.Matricula,
            Nombre: req.query.Nombre,
            Domicilio: req.query.Domicilio,
            Sexo: req.query.Sexo,
            Especialidad: req.query.Especialidad
        }
    })
        .then(function (response) {
            res.redirect('/')
        });
})









module.exports = router;